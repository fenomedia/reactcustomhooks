# Getting Started

Das Projekt wurde erstellt mit [Create React App](https://github.com/facebook/create-react-app).

## Skripte

Im Projektverzeichnis folgenden Befehl eingeben, um den Server zu starten

### `yarn start`

Um die Applikation im Entwicklungsmodus zu starten.\
Öffne [http://localhost:3000](http://localhost:3000) um die Seite im Browser zu sehen.

Die Seite lädt automatisch neu, wenn Codeänderungen gemacht werden.\
Außerdem sieht man die Linter-Fehler in der Console.

## Erklärung von Custom Hooks anhand von useReducer

Dieses Projekt dient lediglich der Veranschaulichung von Custom Hooks am Beispiel von useReducer. Das ist eine Adaption des useReducer Hooks von Facebook.
