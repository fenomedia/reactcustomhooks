import React, { useState } from 'react'
import useReducer from './useReducer'

/**
 * Logik zum Manipulieren eines States
 *
 * @param array state
 * @param string action
 * @returns array
 */

const reducer = (state, action) => {
  const { type, name } = action
  switch (type) {
    case 'add':
      return [
        ...state,
        {
          name,
        },
      ]
    default:
      return state
  }
}

function Todo() {
  const [todos, dispatch] = useReducer(reducer, [])
  const [newTodo, setNewTodo] = useState('')

  const addTodo = (e) => {
    e.preventDefault()

    dispatch({ type: 'add', name: newTodo })
  }
  return (
    <>
      {todos.map((todo, i) => (
        <div key={`todo_${i}`}>
          <span>{todo.name}</span>
          <button>löschen</button>
        </div>
      ))}
      <form onSubmit={addTodo}>
        <input
          type="text"
          value={newTodo}
          onChange={({ target }) => setNewTodo(target.value)}
          name="name"
          placeholder="z.B. Aufwaschen"
        />
        <button>hinzufügen</button>
      </form>
    </>
  )
}

export default Todo
