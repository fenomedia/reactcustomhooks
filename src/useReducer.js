import { useState } from 'react'

/**
 * Managed und hält den State
 * Wendet über dispatch den reducer an und speichert den neuen State
 * Gibt den State und die Dispatch Funktion für Komponenten zurück
 *
 * Somit kann eine beliebige Logik und ein State an useReducer übergeben werden
 * und somit von der Komponente entkoppelt werden
 *
 * Der useReducer Hook ist eine Adaption von FB useReducer
 * @see https://reactjs.org/docs/hooks-reference.html#usereducer
 *
 * @param function reducer
 * @param any initialState
 * @returns array
 */

function useReducer(reducer, initialState) {
  const [state, setState] = useState(initialState)

  const dispatch = (action) => {
    const nextState = reducer(state, action)
    setState(nextState)
  }

  return [state, dispatch]
}

export default useReducer
